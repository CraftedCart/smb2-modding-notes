SMB2 Modding Notes
==================

## Brief
Various notes about hacking Super Monkey Ball 2

## Discord
SMB Level Workshop: https://discord.gg/CEYjvDj

## Also see...
- https://github.com/ComplexPlane/romhacking-notes
- https://gitlab.com/CraftedCart/smbinfo
