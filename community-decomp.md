Community decompilation
=======================

## Brief
- A Ghidra server has been set up hosting a Super Monkey Ball 2 disassembly/decompilation
- Ask ComplexPlane for access

## Connecting to the Ghidra server
- Create a new shared project with the address of the server, and the default port of `13100`
- Log in with your username and password (The default password is `changeme` and it expires after 24 hours)

## Useful Ghidra plugins/extensions
- https://github.com/aldelaro5/ghidra-gekko-broadway-lang
  - Adds support for the PowerPC Gekko processor
- https://github.com/Cuyler36/Ghidra-GameCube-Loader
  - Adds a program loader for DOL and REL files
- https://gitlab.com/CraftedCart/ghidra-supermonkeyballtools
  - Adds a converter between Ghidra, REL/DOL, and GameCube RAM addresses
  - Can only be trusted for .text and .data0 sections at the moment
  - Other .data sections tend to be slightly off
  - .bss (uninitialized) sections are completely off for now

## Style conventions
- In short: Rust style with some additions
- Symbols may be prefixed with `g_` to indicate that they are a guess (**`g_` does not mean global!**)
- Uncertainty in symbol names is ok, preferably prefixed with `g_`
  - Eg: `uint g_something_to_do_with_cutscenes(int g_cutscene_id)`

### Naming
- Symbols that are part of the GameCube SDK should use the GameCube SDK names, and not be adjusted to follow the below
- Structs use `PascalCase`
- Functions use `snake_case`
- Variables use `snake_case`
- Constants use `SCREAMING_SNAKE_CASE`
- Prefer using typedefs such as `u32`, `s16`, `u8`, etc. over `uint`, `short`, `uchar`, etc.

### Comments
- Prefer long lines + word wrapping enabled in Ghidra over manually wrapping lines
- Function comments should go as plate comments

## About auto-generated symbol names...
- When the program was imported into Ghidra, a symbol map (auto-generated from Dolphin Emulator) was imported when loading main.dol
- While this has been helpful, do watch out for incorrectly identified symbols
- Getter/setter functions are notable, often being labeled as `ipc::IPCGetBufferLo`
- Also auto-generated namespace names are a mess

## Also see...
- https://github.com/ComplexPlane/romhacking-notes/blob/master/community-decomp.md
