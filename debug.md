Debug
=====

## Brief
- SMB2 has a debug DIP switch, which can be enabled with the following AR code
```
185bc473 00000003
005bc473 00000003
```

- Pressing start on the splash screen will now bring up a menu with some options (including game start, a stage select, test mode, and more)
- L + Start can be pressed to bring up a debug overlay menu at any time
- R + Start can be pressed to pause the game at any time (This is different from pressing start in-game which brings up a pause menu)
