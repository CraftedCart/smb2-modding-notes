Events
======

## Brief
The game has various "events", each with a state flag, as well as activate/tick/deactivate functions. An array of events can be found at `0x80370990` in Ghidra.

## C code
```c
// Event IDs
// These are just indices of the event array
#define EVENT_STAGE 0
#define EVENT_WORLD 1
#define EVENT_BALL 2
#define EVENT_APE 3
#define EVENT_STOBJ 4
#define EVENT_ITEM 5
#define EVENT_RECPLAY 6
#define EVENT_OBJ_COLLISION 7
#define EVENT_NAME_ENTRY 8
#define EVENT_INFO 9
#define EVENT_COURSE 10
#define EVENT_VIBRATION 11
#define EVENT_COMMEND 12
#define EVENT_VIEW 13
#define EVENT_EFFECT 14
#define EVENT_MINIMAP 15
#define EVENT_CAMERA 16
#define EVENT_SPRITE 17
#define EVENT_MOUSE 18
#define EVENT_SOUND 19
#define EVENT_BACKGROUND 20
#define EVENT_REND_EFC 21
#define EVENT_ADX 22

#define EVENT_NUM 23

// Yes these are all typedefs to the same type
typedef void (*EventActivateFunc)();
typedef void (*EventTickFunc)();
typedef void (*EventDeactivateFunc)();

/** Status codes for events, the pause state, the ball struct, and more in the game. One byte in size. */
enum Status {
    STAT_NULL, ///< Inactive
    STAT_INIT, ///< Often used for a frame or less when transitioning from STAT_NULL to STAT_NORMAL
    STAT_NORMAL, ///< Active
    STAT_DEST, ///< Used when a destination is reached (maybe used at the end of a cutscene shot?)
    STAT_FREEZE, ///< Paused (Eg: The WORLD event is marked as STAT_FREEZE during the stage spin-in)
    STAT_INIT_FIRST, ///< Unknown
    STAT_GOAL_INIT, ///< Unknown
    STAT_GOAL, ///< Unknown
    STAT_RINGOUT_INIT, ///< Unknown
    STAT_RINGOUT, ///< Unknown
    STAT_MINI_MODE_0_INIT, ///< Unknown
    STAT_MINI_MODE_0, ///< Unknown
};

typedef struct Event {
    /** A status code for the event */
    Status status;

    /** The name of the event */
    char *name;

    /** Called when transitioning status from STAT_NULL to STAT_NORMAL */
    EventActivateFunc event_activate_func;

    /** Called every frame from the main game loop if the event status is STAT_NORMAL */
    EventTickFunc event_tick_func;

    /** Called when transitioning from any status code to STAT_NULL */
    EventDeactivateFunc event_deactivate_func;

    /** How long the event takes to tick (in an unknown time unit) - used for debug performance metrics */
    u32 tick_time;
} Event;

/**
 * Deactivates an event by calling its deactivate function, then setting its status to `STAT_NULL`. This function has no
 * effect if called with an event whose status is already `STAT_NULL`.
 */
void event_deactivate(u32 event_id) {
    if (events[event_id].status != STAT_NULL) {
        events[event_id].event_deactivate_func();
        events[event_id].status = STAT_NULL;
    }
}

/**
 * Activates an event by calling its activate function, then setting its status to `STAT_NORMAL`. For events whose
 * statuses are not `STAT_NULL`, they will be deactivated first, before being activated.
 */
void event_activate(u32 event_id) {
    if (events[event_id].status != STAT_NULL) {
        event_deactivate(event_id);
    }
    events[event_id].event_activate_func();
    events[event_id].status = STAT_NORMAL;
}

/**
 * Unfreezes a frozen event (Changes an events status from `STAT_FREEZE` to `STAT_NORMAL`). This does not call the activation
 * function. Calling this function with a non-frozen event will log an error and not change the event status.
 */
void ev_restart(u32 event_id) {
    if (events[event_id].status == STAT_FREEZE) {
        events[event_id].status = STAT_NORMAL;
    } else {
        printf("ev_restart: event %s is not suspended\n", events[event_id].name);
    }
}

/**
 * Ticks all events with status `STAT_NORMAL`. For events with status `STAT_INIT`, they will be activated and then
 * ticked as usual. For events that have a status of `STAT_DEST`, they will be deactivated.
 *
 * @note Code slightly simplified compared to Ghidra output for readability's sake, but should retain the same
 *       functionality. Simplifications include removing a `goto`; replacing `Event *event = events; do { stuff;
 *       event++; } while (true)` with array indexing; and replacing some weird greater-than/less-than comparisons with
 *       `==` equality comparisons. See `0x8019f008` in Ghidra for the decompiled output.
 */
void tick_events(void) {
    for (u32 i = 0; i < EVENT_NUM; i++) {
        Event *event = events[i];

        perf_init_timer(5);

        Status status = event->status;
        if (status == STAT_NORMAL) {
            event->event_tick_func();
        } else if (status == STAT_INIT) {
            event_activate(i);
            event->event_tick_func();
        } else if (status == STAT_DEST) {
            event_deactivate(i);
        }

        u32 elapsed_time = perf_stop_timer(5);
        event->tick_time = elapsed_time;
    }
}
```

## In-game debug menus
There's a couple of debug menus in-game for getting information on events. The debug overlay has an `Event` sub-menu for seeing event statuses. There is also the `Performance > Event` sub-menu for seeing event ticking performance metrics.

![Event debug overlays](img/events-debug-overlay.png)

## Events

### `EVENT_STAGE`: `0`

### `EVENT_WORLD`: `1`
This event's status is marked as `STAT_FREEZE` during the level spin-in

### `EVENT_BALL`: `2`

### `EVENT_APE`: `3`

### `EVENT_STOBJ`: `4`

### `EVENT_ITEM`: `5`

### `EVENT_RECPLAY`: `6`

### `EVENT_OBJ_COLLISION`: `7`

### `EVENT_NAME_ENTRY`: `8`

### `EVENT_INFO`: `9`

### `EVENT_COURSE`: `10`

### `EVENT_VIBRATION`: `11`
Something related to controller rumble motors

### `EVENT_COMMEND`: `12`

### `EVENT_VIEW`: `13`
Activating this goes into view-stage when in gameplay

### `EVENT_EFFECT`: `14`

### `EVENT_MINIMAP`: `15`

### `EVENT_CAMERA`: `16`

### `EVENT_SPRITE`: `17`
Presumably related to UI elements.

### `EVENT_MOUSE`: `18`
Related to the mouse cursor you get when pressing B + X when debug paused. This cursor lets you move sprites (UI elements) around on-screen.

### `EVENT_SOUND`: `19`

### `EVENT_BACKGROUND`: `20`

### `EVENT_REND_EFC`: `21`

### `EVENT_ADX`: `22`
