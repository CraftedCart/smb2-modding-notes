Heaps
=====

## Brief
- Super Monkey Ball 2 makes use of various memory heaps
  - `mainHeap`
  - `stageHeap`
  - `backgroundHeap`
  - `charaHeap`
  - `replayHeap`
- `mainHeap` is usually the current heap (`OSSetCurrentHeap`)
  - The main heap is what is used by `OSAlloc` / `OSFree`
- If the heap handles are set to `-1`, they do not exist

## Debug menu
- There is a debug menu showing heap sizes and the numbers of bytes free, accessible from L + Start
- This requires debug mode enabled (See debug.md)

## Bootup
- `mainHeap` is created
- `mainHeap` is destroyed and recreated
- Game heaps are created (including `mainHeap`)
- **The AV logo plays**
- All game heaps are destroyed
- Game heaps are created again
- **I really love bananas**
- All game heaps are destroyed
- Game heaps are created again
- **SMB2 splash screen**
- All game heaps destroyed again
- Game heaps are created again
- **Main menu**

## On REL switching
- When switching REL (with the exception of loading test mode), all game heaps seem to be destroyed and recreated
- This includes...
  - Loading story mode file select
  - Launching into the first challenge mode stage
  - Launching into a practice mode stage
  - Launching into a party game
  - And more

## `mainHeap` fallback
- Under some condition, the game can fallback to using `mainHeap` for all specialized allocations
- See `8019b31c` in Ghidra
```c
  if ((*(uint *)(&DAT_8036e340 + iVar1) & 2) != 0) {
    if (stageHeap < 0) {
      stageHeap = mainHeap;
    }
    if (backgroundHeap < 0) {
      backgroundHeap = mainHeap;
    }
    if (charaHeap < 0) {
      charaHeap = mainHeap;
    }
    if (replayHeap < 0) {
      replayHeap = mainHeap;
    }
  }
```
