Miscellaneous notes that live here until I can find some place appropriate to put them
======================================================================================
(or perhaps more likely they'll just stay here forever)

# Ape skeletons
- `/ape/skl.arc` is a U.8 archive
  - The contents of these archives can be viewed with the `wszst` tool (https://szs.wiimm.de/wszst/)
  - http://wiki.tockdom.com/wiki/U8_(File_Format)

```
$ ./bin/wszst list ~/SMBHacking/dolphincleanroot/files/ape/skl.arc

* Files of U8:/home/alice/SMBHacking/dolphincleanroot/files/ape/skl.arc

aiai.skl
aiai_face.skl
aiai_handl.skl
aiai_handr.skl
baba.skl
baba_face.skl
baba_handl.skl
baba_handr.skl
baby.skl
baby_face.skl
baby_handl.skl
baby_handr.skl
dom.skl
female.skl
female_face.skl
female_handl.skl
female_handr.skl
gongon.skl
gongon_face.skl
gongon_handl.skl
gongon_handr.skl
jiji.skl
jiji_face.skl
jiji_handl.skl
jiji_handr.skl
kobspa.skl
kobspa_face.skl
kobspa_handl.skl
kobspa_handr.skl
kobun.skl
kobun_face.skl
kobun_handl.skl
kobun_handr.skl
kumo.skl
lion.skl
madcrs.skl
madcrs_face.skl
madcrs_handl.skl
madcrs_handr.skl
madfrg.skl
madfrg_face.skl
madfrg_handl.skl
madfrg_handr.skl
madh.skl
madh_face.skl
madh_handl.skl
madh_handr.skl
madh_tail.skl
madnkd.skl
madnkd_face.skl
madnkd_handl.skl
madnkd_handr.skl
madnkd_tail.skl
madspa.skl
madspa_face.skl
madspa_handl.skl
madspa_handr.skl
male.skl
male_face.skl
male_handl.skl
male_handr.skl
meemee.skl
meemee_face.skl
meemee_handl.skl
meemee_handr.skl
rob.skl
rob_face.skl
rob_handl.skl
rob_handr.skl
skel.ska
tako.skl
whale.skl
whale_face.skl
whale_handl.skl
whale_handr.skl
```
